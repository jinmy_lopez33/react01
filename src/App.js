import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import Header from './components/layouts/Header';
import Footer from './components/layouts/Footer';
import Card from './components/component/Card';
//import Container from './components/layouts/Container';
//import { todos } from './todos.json';
// subcomponents
import TodoForm from './components/TodoForm';

// DB
import firebase from 'firebase';
import {DB_CONFIG} from './config/config';

class App extends Component {
  constructor(){
    super();
    this.state = {
      todos: []
    };
    this.addNote = this.addNote.bind(this);
		this.removeNote = this.removeNote.bind(this);

    this.app = firebase.initializeApp(DB_CONFIG);
    this.db = this.app.database().ref().child('Cards');
    //this.handleAddTodo = this.handleAddTodo.bind(this);


  }
  
  componentDidMount() {
		const { todos } = this.state;
		this.db.on('child_added', snap => {
			todos.push({
				noteId: snap.key,
				noteContent: snap.val().noteContent
      });      
      
      this.setState({todos});
      
		});

		this.db.on('child_removed', snap => {
			for(let i = 0; i < todos.length; i++) {
				if(todos[i].noteId === snap.key) {
					todos.splice(i , 1);
				}
			}
			console.log(todos);
			this.setState({todos});
		});

  }
  addNote(todo) {
		this.db.push().set({noteContent: todo});
  }	

	removeNote(noteId) {
		this.db.child(noteId).remove();
	}

  render() {
    return (
      <div className="App">
        <Header className="" task={this.state.todos.length} />       
          <main class="wrap">
          <section>
              Ociones de sistema
            </section>
            <section class="search">
              Buscar
            </section>
            <section>
              <nav>
                <ul class="menu-options">
                  <li>Equipaje</li>
                  <li>Facturacion electrónica</li>
                  <li>Pasajes</li>
                  <li>Ventas</li>
                  <li>Venta web</li>
                </ul>
              </nav>
            </section>
            <section>
              <div class="content-options"> 
                <div>  
                  holi              
                </div>
              </div>
              <div class="content-options"> 

              </div>
            </section>
          </main>       
        <Footer />
      </div>
    );
  }
}

export default App;