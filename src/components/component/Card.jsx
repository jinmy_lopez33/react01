import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Card extends Component {
	
	constructor(props) {
		super(props);
		this.noteContent = props.noteContent;
		this.noteId = props.noteId;		
	}

	handleRemoveNote(id) {
		this.props.removeNote(id);
	}

	render(props) {	

		return (
			<div className="uk-card uk-card-default uk-card-body">
				<h3 className="uk-card-title">{this.noteContent.title}</h3>
				<span className="uk-badge">{this.noteContent.priority}</span>
				<p>{this.noteContent.description}</p>
				<p>{this.noteContent.responsable}</p>
				<button
					className="uk-button uk-button-danger"
					onClick={() => this.handleRemoveNote(this.noteId)}>
					Delete
				</button>
        	</div>
		)
	}
}

Card.PropTypes = {
	noteContent: PropTypes.String	
};

export default Card;