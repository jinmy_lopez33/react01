import React, { Component } from 'react';

class Footer extends Component {
render() {
return (
    <nav className="uk-navbar-container">
        <div className="uk-navbar-left uk-background-muted">

            <ul className="uk-navbar-nav">
                <li className="uk-active"><a href="#holi">holi</a></li>
                <li>
                    <a href="#parent">Parent</a>
                    <div className="uk-navbar-dropdown">
                        <ul className="uk-nav uk-navbar-dropdown-nav">
                            <li className="uk-active"><a href="#">holi</a></li>
                            <li><a href="#">Item</a></li>
                            <li><a href="#">Item</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="#">Item</a></li>
            </ul>
        </div>
    </nav>
);
}
}
export default Footer;