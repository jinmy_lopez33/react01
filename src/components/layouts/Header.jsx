import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../media/css/layouts/navbar.css';

class Header extends Component {
    static propTypes = {
        task: PropTypes.number.isRequired
    }
render() {
    const {task} = this.props;
    return (
        <nav className="uk-navbar-container uk-navbar">
            <div className="uk-navbar-left uk-background-primary uk-light">
                <ul className="uk-navbar-nav">
                    <li className="uk-active"><a href="#">Active</a></li>
                    <li>
                        <a href="#">Parent</a>
                        <div className="uk-navbar-dropdown">
                            <ul className="uk-nav uk-navbar-dropdown-nav">
                                <li className="uk-active"><a href="#">holi</a></li>
                                <li><a href="#">Item</a></li>
                                <li><a href="#">Item</a></li>
                                <li><a href="#"></a></li>
                            </ul>
                        </div>
                    </li>                           
                </ul>
            </div>
            <div className="uk-navbar-right uk-background-primary uk-light">
                <ul className="uk-navbar-nav">
                    <li className="uk-active"><a href="#">TAREAS</a></li>
                    <li><a href="#"><span className="uk-badge">{task}</span></a></li> 
                </ul>
            </div>
        </nav>
    );
}
}

export default Header;