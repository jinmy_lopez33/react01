import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';
import Header from './components/layouts/Header';
import Footer from './components/layouts/Footer';
import Card from './components/component/Card';
//import Container from './components/layouts/Container';
//import { todos } from './todos.json';
// subcomponents
import TodoForm from './components/TodoForm';

// DB
import firebase from 'firebase';
import {DB_CONFIG} from './config/config';

class App extends Component {
  constructor(){
    super();
    this.state = {
      todos: []
    };
    this.addNote = this.addNote.bind(this);
		this.removeNote = this.removeNote.bind(this);

    this.app = firebase.initializeApp(DB_CONFIG);
    this.db = this.app.database().ref().child('Cards');
    //this.handleAddTodo = this.handleAddTodo.bind(this);


  }
  
  componentDidMount() {
		const { todos } = this.state;
		this.db.on('child_added', snap => {
			todos.push({
				noteId: snap.key,
				noteContent: snap.val().noteContent
      });      
      
      this.setState({todos});
      
		});

		this.db.on('child_removed', snap => {
			for(let i = 0; i < todos.length; i++) {
				if(todos[i].noteId === snap.key) {
					todos.splice(i , 1);
				}
			}
			console.log(todos);
			this.setState({todos});
		});

  }
  addNote(todo) {
		this.db.push().set({noteContent: todo});
  }	

	removeNote(noteId) {
		this.db.child(noteId).remove();
	}

  render() {
    return (
      <div className="App">
        <Header className="" task={this.state.todos.length} />         
        <div className="App-body">           
        <TodoForm onAddTodo={this.addNote}></TodoForm>         
        {
						this.state.todos.map(todo => {
							return (
								<Card 
									noteContent={todo.noteContent} 
                  noteId={todo.noteId}
                  key={todo.noteId}
									removeNote={this.removeNote}
								/>)
						})
					}     
        </div>         
        <Footer />
      </div>
    );
  }
}

export default App;